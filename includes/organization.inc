<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_organization_get_species($org_id) {

  $species_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('species_id'))
    ->condition('organization_id', $org_id)
    ->execute();
  $or = db_or();
  while (($species_id = $species_id_query->fetchObject())) {
    $or->condition('organism_id', $species_id->species_id);
  }
  $species_query = db_select('chado.organism', 'o')
    ->fields('o', array('organism_id', 'abbreviation', 'genus', 'species', 'common_name'))
    ->condition($or)
    ->execute();

  $species = array();
  while (($species_result = $species_query->fetchObject())) {
    $species[$species_result->organism_id] = $species_result;
  }
  return $species;
}

/**
 *
 */
function gttn_profile_organization_get_methods($org_id) {
  $method_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('method_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  $or = db_or();
  while (($method_id = $method_id_query->fetchObject())) {
    $or->condition('method_id', $method_id->method_id);
  }

  $method_query = db_select('gttn_profile_method', 'm')
    ->fields('m', array('method_id', 'name', 'description'))
    ->condition($or)
    ->execute();

  $methods = array();
  while (($method_record = $method_query->fetchObject())) {
    $methods[] = $method_record;
  }
  return $methods;
}

/**
 *
 */
function gttn_profile_organization_get_products($org_id) {
  $product_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('product_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  $or = db_or();
  while (($product_id = $product_id_query->fetchObject())) {
    $or->condition('product_id', $product_id->product_id);
  }

  $product_query = db_select('gttn_profile_product', 'p')
    ->fields('p', array('product_id', 'name', 'hs_code'))
    ->condition($or)
    ->execute();

  $products = array();
  while (($product_record = $product_query->fetchObject())) {
    $products[] = $product_record;
  }
  return $products;
}

/**
 *
 */
function gttn_profile_organization_get_regions($org_id) {
  $region_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('region_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  $or = db_or();
  while (($region_id = $region_id_query->fetchObject())) {
    $or->condition('region_id', $region_id->region_id);
  }

  $region_query = db_select('gttn_profile_region', 'p')
    ->fields('p', array('region_id', 'name'))
    ->condition($or)
    ->execute();

  $regions = array();
  while (($region_record = $region_query->fetchObject())) {
    $regions[] = $region_record;
  }
  return $regions;
}

/**
 *
 */
function gttn_profile_organization_details($org_id) {

  $output = "";
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o')
    ->condition('organization_id', $org_id)
    ->execute()->fetchObject();

  // General Information.
  $output .= "<h2>General Information</h2>";
  $rows[] = array('Name', $org->name);
  $rows[] = array('Address Line 1', $org->address_1);
  if (!empty($org->address_2)) {
    $rows[] = array('Address Line 2', $org->address_2);
  }
  $rows[] = array('City', $org->city);
  if (!empty($org->region)) {
    $rows[] = array('State/Province', $org->region);
  }
  $rows[] = array('Country', $org->country);
  $rows[] = array('Website', "<a target=\"blank\" href=\"http://{$org->website}\">{$org->website}</a>");

  $vars = array(
    'header' => array(
      'Attribute', 'Value',
    ),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );
  $output .= theme_table($vars);

  // TODO: supported species/methods/products/regions.
  // Properties.
  $output .= "<h2>Properties</h2>";
  $property_query = db_select('gttn_profile_organization_property', 'op');
  $property_query->join('gttn_profile_property', 'p', 'p.property_id = op.property_id');
  $property_query->fields('op', array('value'))
    ->fields('p', array('name', 'description'))
    ->condition('organization_id', $org_id);
  $property_query = $property_query->execute();

  $rows = array();
  while (($property = $property_query->fetchObject())) {
    $name = $property->description;
    $value = empty($property->value) ? "Yes" : $property->value;

    $row = array($name, $value);
    $rows[$property->name] = $row;
  }

  $vars = array(
    'header' => array(
      'Property Name', 'Value',
    ),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );

  $output .= theme_table($vars);
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  return $output;
}

/**
 *
 */
function gttn_profile_organization_members($org_id) {
  $per_page = 20;
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o')
    ->condition('organization_id', $org_id)
    ->execute()->fetchObject();

  $members = db_select('gttn_profile_organization_members', 'm')
    ->fields('m');
  $members->join('chado.contact', 'c', 'c.contact_id = m.contact_id');
  $members = $members->fields('c', array('name'))
    ->condition('organization_id', $org_id)
    ->execute();

  $rows = array();
  while (($member = $members->fetchObject())) {
    $name = $member->name;
    $row = array($name);
    $rows[$name] = $row;
  }
  ksort($rows);

  $current_page = pager_default_initialize(count($rows), $per_page);
  $chunks = array_chunk($rows, $per_page, TRUE);
  $vars = array(
    'header' => array(
      'name',
    ),
    'rows' => isset($chunks[$current_page]) ? $chunks[$current_page] : NULL,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );

  $output = theme_table($vars);

  $output = theme('pager', array('quantity', count($rows))) . $output;
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  return $output;
}

/**
 * This function displays organizations based on a search and provides a search form.
 *
 * @global string $base_url The base url of the site.
 * @global stdClass $user The user trying to browse organizations.
 *
 * @return string
 */
function gttn_profile_organization_display() {
  global $base_url, $user;

  $per_page = 20;

  $query = db_select('gttn_profile_support', 's')
    ->fields('s', array('organization_id'));
  $and = db_and();

  if (!empty($_GET['method'])) {
    $methods = explode(';', $_GET['method']);
    $method_or = db_or();
    foreach ($methods as $method) {
      $method_or->condition('name', '%' . db_like($method) . '%', 'LIKE');
    }
    $method_query = db_select('gttn_profile_method', 'm')
      ->fields('m', array('method_id'))
      ->condition($method_or)
      ->execute();
    $or = db_or();
    $empty_or = TRUE;
    while (($method = $method_query->fetchObject())) {
      $or->condition('method_id', $method->method_id);
      $empty_or = FALSE;
    }
    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('method_id', 1)
        ->condition('method_id', 0);
    }
  }
  if (!empty($_GET['product'])) {
    $products = explode(';', $_GET['product']);
    $product_or = db_or();
    foreach ($products as $product) {
      $product_or->condition('name', '%' . db_like($product) . '%', 'LIKE');
    }
    $product_query = db_select('gttn_profile_product', 'p')
      ->fields('p', array('product_id'))
      ->condition($product_or)
      ->execute();
    $or = db_or();
    $empty_or = TRUE;
    while (($product = $product_query->fetchObject())) {
      $or->condition('product_id', $product->product_id);
      $empty_or = FALSE;
    }
    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('product_id', 1)
        ->condition('product_id', 0);
    }
  }
  if (!empty($_GET['region'])) {
    $regions = explode(';', $_GET['region']);
    $region_or = db_or();
    foreach ($regions as $region) {
      $region_or->condition('name', '%' . db_like($region) . '%', 'LIKE');
    }
    $region_query = db_select('gttn_profile_region', 'p')
      ->fields('p', array('region_id'))
      ->condition($region_or)
      ->execute();
    $or = db_or();
    $empty_or = TRUE;
    while (($region = $region_query->fetchObject())) {
      $or->condition('region_id', $region->region_id);
      $empty_or = FALSE;
    }
    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('region_id', 1)
        ->condition('region_id', 0);
    }
  }
  if (!empty($_GET['species'])) {
    $species = explode(';', $_GET['species']);
    $or = db_or();
    $empty_or = TRUE;
    foreach ($species as $item) {
      $item = explode(' ', $item);
      $cond = db_and();
      if (!empty($item[1])) {
        $cond->condition('genus', '%' . db_like($item[0]) . '%', 'LIKE')
          ->condition('species', '%' . db_like($item[1]) . '%', 'LIKE');
      }
      else {
        $cond = db_or()
          ->condition('genus', '%' . db_like($item[0]) . '%', 'LIKE')
          ->condition('species', '%' . db_like($item[0]) . '%', 'LIKE');
      }
      $organism_query = db_select('chado.organism', 'o')
        ->fields('o', array('organism_id'))
        ->condition($cond)
        ->execute();
      while (($organism_id = $organism_query->fetchObject())) {
        $or->condition('species_id', $organism_id->organism_id);
        $empty_or = FALSE;
      }
    }

    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('species_id', 1)
        ->condition('species_id', 0);
    }
  }
  if ($and->count() > 0) {
    $query->condition($and);
  }

  $species_str = isset($species) ? implode(';', $species) : "";
  $species_form = "<div class=\"row\"><div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-species\">Species:</label><br>"
        . "<input id=\"gttn-org-species\" type=\"text\" name=\"species\" value=\"{$species_str}\"></div>";
  $method = isset($methods) ? implode(';', $methods) : "";
  $method_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-method\">Methods:</label><br>"
        . "<input id=\"gttn-org-method\" type=\"text\" name=\"method\" value=\"{$method}\"></div>";
  $product = isset($products) ? implode(';', $products) : "";
  $product_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-product\">Products:</label><br>"
        . "<input id=\"gttn-org-product\" type=\"text\" name=\"product\" value=\"{$product}\"></div>";
  $region = isset($regions) ? implode(';', $regions) : "";
  $region_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-region\">Regions:</label><br>"
        . "<input id=\"gttn-org-region\" type=\"text\" name=\"region\" value=\"{$region}\"></div></div>";
  $search_button = "<input style=\"margin-top:20px;margin-bottom:20px;\" type=\"submit\" id=\"gttn-org-search\" class=\"form-submit\" value=\"Search\">";

  $search_form = $species_form . $method_form . $product_form . $region_form . $search_button;

  $query = $query->execute();

  $rows = array();
  while (($org_id = $query->fetchObject())) {
    $result = db_select('gttn_profile_organization', 'o')
      ->fields('o')
      ->condition('organization_id', $org_id->organization_id)
      ->range(0, 1)
      ->execute()->fetchObject();
    $org_id = $result->organization_id;
    $name = $result->name;

    if (gttn_profile_access('browse_organization_details')) {
      $name = "<a href=\"$org_id\">$name</a>";
    }

    $site = "<a target=\"blank\" href=\"http://{$result->website}\">{$result->website}</a>";

    $species = implode(", ", array_map(
          function ($data) {
              return "{$data->genus} {$data->species}";
          },
          gttn_profile_organization_get_species($org_id)
      ));

    $methods = array();
    foreach (gttn_profile_organization_get_methods($org_id) as $method) {
      $methods[] = $method->name;
    }
    $methods = implode(", ", $methods);
    $products = array();
    foreach (gttn_profile_organization_get_products($org_id) as $product) {
      $products[] = $product->name;
    }
    $products = implode(", ", $products);
    $regions = array();
    foreach (gttn_profile_organization_get_regions($org_id) as $region) {
      $regions[] = $region->name;
    }
    $regions = implode(", ", $regions);

    $row = array($name, $site, $species, $methods, $products, $regions);
    if (gttn_profile_access('edit_organization', $org_id)) {
      $row[] = "<a href=\"edit/$org_id\">Edit Organization</a>";
    }
    $rows[$name] = $row;
  }

  ksort($rows);

  $current_page = pager_default_initialize(count($rows), $per_page);
  $chunks = array_chunk($rows, $per_page, TRUE);

  $vars = array(
    'header' => array(
      'Name', 'Website', 'Supported species', 'Supported methods', 'Supported products', 'Supported regions',
    ),
    'rows' => isset($chunks[$current_page]) ? $chunks[$current_page] : NULL,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );

  $output = theme_table($vars);

  $output = $search_form . theme('pager', array('quantity', count($rows))) . $output;
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  return $output;
}

/**
 *
 */
function gttn_profile_organization_new_member($org_id, $uid, $contact_id) {

  $and = db_and()
    ->condition('uid', $uid)
    ->condition('organization_id', $org_id);
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition($and)
    ->execute();
  // If the user was already a member, then do nothing.
  if (($query->fetchObject())) {
    return;
  }

  gttn_profile_create_record('gttn_profile_organization_members', array(
    'contact_id' => $contact_id,
    'uid' => $uid,
    'organization_id' => $org_id
  ));

  gttn_profile_organization_notify($org_id, $uid, $contact_id, 'new_member');
}

/**
 *
 */
function gttn_profile_organization_remove_member($org_id, $uid, $contact_id) {

  $and = db_and()
    ->condition('uid', $uid)
    ->condition('organization_id', $org_id);
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition($and)
    ->execute();
  // If the user was not already a member, then do nothing.
  if (!($query->fetchObject())) {
    return;
  }

  db_delete('gttn_profile_organization_members')
    ->condition($and)
    ->execute();

  gttn_profile_organization_notify($org_id, $uid, $contact_id, 'remove_member');
}

/**
 *
 */
function gttn_profile_organization_notify($org_id, $uid, $contact_id, $type) {

  $and = db_and()
    ->condition('organization_id', $org_id)
    ->condition('is_primary_contact', TRUE);
  $primary_id = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition($and)
    ->execute()->fetchObject()->uid;

  if ($type == 'new_member') {
    $subject = 'GTTN: Pending membership claim';
  }
  if ($type == 'remove_member') {
    $subject = 'GTTN: Organization member removed';
  }

  $primary = user_load($primary_id);
  $to = $primary->mail;
  $lang = user_preferred_language($primary);
  $params = array(
    'subject' => $subject,
    'org_id' => $org_id,
    'primary_contact' => $primary,
    'user' => user_load($uid),
    'user_contact_id' => $contact_id,
  );
  $from = variable_get('site_mail');

  drupal_mail('gttn_profile', $type, $to, $lang, $params, $from, TRUE);
}

/**
 *
 */
function gttn_profile_organization_approve_member($uid, $org_id) {

  global $user;

  if ($user->uid == 0) {
    $dest = drupal_get_destination();
    drupal_goto('user/login', array('query' => $dest));
  }
  elseif (!gttn_profile_access('approve_organization_members', $org_id)) {
    drupal_access_denied();
    return;
  }

  $and = db_and()
    ->condition('uid', $uid)
    ->condition('organization_id', $org_id);
  $query = db_update('gttn_profile_organization_members')
    ->fields(array(
      'status' => 1,
    ))
    ->condition($and)
    ->execute();

  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  $output = "";
  drupal_set_message(t('Congratulations, the user has been approved!'));
  return drupal_render($output);
}
