<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_load_defaults(&$form, &$form_state) {
  if (!empty($form_state['saved_values'][$form_state['stage']])) {
    foreach ($form_state['saved_values'][$form_state['stage']] as $key => $val) {
      if (empty($form_state['values'][$key]) and !empty($form[$key])) {
        $form[$key]['#default_value'] = $val;
      }
    }
  }
}
