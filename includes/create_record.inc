<?php

/**
 * @file
 */

/**
 * This function tries to write a record to the table specified. If the record already
 * exists, then it is updated. Otherwise, a new record is inserted.
 *
 * @param string $table
 *   The table being written to.
 * @param mixed $records
 *   Either a single record object or an array of record objects.
 *
 * @return integer The primary key id of the updated or inserted record.
 */
function gttn_profile_create_record($table, $records, $options = array()) {
  if (!array_key_exists('multi', $options)) {
    $options['multi'] = FALSE;
  }

  if (!$options['multi']) {
    $records = array($records);
  }
  else {
    $has_fields = FALSE;
    $insert = db_insert("$table");
    $fields_arr = array();
    foreach ($records[0] as $field => $val) {
      $fields_arr[] = $field;
    }
    $insert->fields($fields_arr);
  }

  if (preg_match('/^chado\.(.*)$/', $table, $matches)) {
    $table_desc = chado_get_schema($matches[1]);
  }
  else {
    $table_desc = drupal_get_schema($table);
  }

  if (!$table_desc) {
    return FALSE;
  }
  $primary = !empty($table_desc['primary key'][0]) ? $table_desc['primary key'][0] : NULL;
  foreach ($records as $values) {
    // Populate insert_values array and check if a record with the same unique
    // fields already exists. If it does, return the existing primary key.
    $insert_values = array();

    foreach ($values as $field => $value) {
      if (is_array($value) and preg_match('/^chado\./', $table)) {
        $results = chado_schema_get_foreign_key($table_desc, $field, $value);
        if (count($results) != 1) {
          return FALSE;
        }
        else {
          $insert_values[$field] = $results[0];
        }
      }
      else {
        $insert_values[$field] = $value;
      }
    }

    $unique = array();
    if (array_key_exists('unique keys', $table_desc)) {
      $unique = $table_desc['unique keys'];
    }
    else {
      $unique = gttn_profile_unique_columns($table);
    }
    $u_cols = array($primary);
    $u_vals = array();

    $exists = FALSE;
    foreach ($unique as $name => $fields) {
      foreach ($fields as $index => $field) {
        $u_cols[] = $field;
        if (!array_key_exists($field, $insert_values)) {
          if (array_key_exists('default', $table_desc['fields'][$field])) {
            $u_vals[$field] = $table_desc['fields'][$field]['default'];
          }
        }
        else {
          $u_vals[$field] = $insert_values[$field];
        }
      }

      if (preg_match('/^chado\.(.*)$/', $table, $matches)) {
        if (($results = chado_select_record($matches[1], $u_cols, $u_vals))) {
          $exists = TRUE;
          // If the new values match with exactly one record, update that one
          // and return the primary key.
          if (!$options['multi'] and count($results) == 1) {
            chado_update_record($matches[1], $u_vals, $insert_values);
            return $results[0]->{$primary};
          }
        }
      }
    }

    if (!$exists and $options['multi']) {
      $insert->values($insert_values);
      $has_fields = TRUE;
    }

    if (!$options['multi']) {
      if ($exists) {
        return $results[0]->{$primary};
      }
      if (preg_match('/^chado\.(.*)$/', $table, $matches)) {
        $new_record = chado_insert_record($matches[1], $insert_values, $options);
        if (!empty($new_record[$primary])) {
          return $new_record[$primary];
        }
      }
      else {
        $id = db_insert($table)
          ->fields($insert_values)
          ->execute();
        if (!empty($id)) {
          return $id;
        }
      }
      return;
    }
  }
  if ($options['multi'] and $has_fields) {
    $insert->execute();
  }
}

/**
 * This function finds the columns that are specified in any UNIQUE constraints
 * for the specified table.
 *
 * @param string $table
 *   The table we want to know the unique columns for.
 *
 * @return array An array of the columns included in the UNIQUE constraint for $table.
 */
function gttn_profile_unique_columns($table) {

  // Separate the table and schema names if possible.
  if (preg_match('/(.*)\.(.*)/', $table, $matches)) {
    $table = $matches[2];
    $schema = $matches[1];
  }
  // Otherwise, the schema is probably public.
  else {
    $schema = 'public';
  }

  // Find the UNIQUE constraint for the table that is not associated with primary key.
  $indexdef = db_query("select indexdef from pg_indexes where tablename = '$table' and schemaname = '$schema' and indexdef LIKE '%UNIQUE%';");

  $unique = array();
  while (($result = $indexdef->fetchObject())) {
    $unique[] = explode(', ', preg_split('/(\(|\))/', $result->indexdef)[1]);
  }
  return $unique;
}
