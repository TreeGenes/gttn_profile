<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_user_chado_update(&$edit, &$account) {
  if ($account->uid != 0) {
    $uid = $account->uid;
  }
  else {
    return;
  }

  if ($account->is_new) {
    $contact_id = gttn_profile_create_record('chado.contact', array(
      'type_id' => array(
        'name' => 'Person',
        'cv_id' => array(
          'name' => 'tripal_contact',
        ),
      ),
      'name' => $edit['full_name'],
    ));
    gttn_profile_create_record('gttn_profile_user_chado', array(
      'uid' => $uid,
      'contact_id' => $contact_id,
    ));
    gttn_profile_create_record('chado.contactprop', array(
      'uid' => $uid,
      'contact_id' => $contact_id,
      'type_id' => array(
        'name' => 'email address',
        'cv_id' => array(
          'name' => 'SIO',
        ),
      ),
      'value' => $account->mail,
    ));
  }
  else {
    $contact_id = db_select('gttn_profile_user_chado', 'u')
      ->fields('u', array('contact_id'))
      ->condition('uid', $uid)
      ->execute()->fetchObject()->contact_id;
  }

  foreach ($edit['org'] as $org_id => $selected) {
    if (!empty($selected)) {
      gttn_profile_organization_new_member($org_id, $uid, $contact_id);
    }
    else {
      gttn_profile_organization_remove_member($org_id, $uid, $contact_id);
    }
  }
}
