Organization Metadata
=====================

This page contains more detailed information about the metadata required during organization registration. For a live-updated document of the metadata fields, formats, and additional notes, please see `SPD Metadata`_.

The Organization Metadata is split up into three main sections: General Information, Analysis Capabilities, and Additional Lab Properties.

General Information
-------------------
 * Name - The name of the organization.
 * Address - The address of the organizaiton, including Line 1, Line 2, Country, City, State/Province, and Postal Code.
 * Website - The website of the organizaiton.
 * Primary Contact Person - The primary contact person for the organization. This person must be a registered GTTN member.

Analysis Capabilities
---------------------
 * Methods - The types of analysis methods that the organization supports. This includes molecular genomics methods, isotope analysis methods, anatomical methods, etc.
 * Products - The products that the organization can perform analysis on. This may include raw timber and wood, paper, musical instruments, furniture, etc.
 * Regions - The geographical regions that the organization can support analysis for. This could include continents, countries, or other regional identifiers.
 * Genus/Species - The Genus/Species that the organization can support analysis for. For each specified genus/species, the organization must also indicate whether it can support all selected analysis methods, products, and regions for that species. If it cannot, then the organization must also indicate which of its selected analysis methods, products, and regions it can support for the species.

Additional Lab Properties
-------------------------
Some additional properties of the organization are only shown when the organization has indicated certain analysis capabilities. For instance, the "Number of Isotope Signatures" property is only applicable when the organization has claimed to be able to support isotope analysis methods, and as a result is only shown when the organization has claimed to be able to support isotope analysis methods.

 * ISO17025 Accreditation
 * Other Accreditations
 * Number of Isotope Signatures
 * Years of experience with timber identification
 * Has the organization given evidence in a court case before?
 * Can you state any references from former service provision?
 * How many samples per week can your organization process?
 * How many business days on average does it take to perform analysis?
 * Does your company have strict confidentiality protocols in place?
 * Do you subcontract parts of service?
 * Do you have any partner organizations?
 * Can you perform analysis on public and private samples?
 * Is there regional restriction to where the service request can originate from?
 * Supported Isotope standards
 * Is your organisation ready to participate in blind tests to validate testing capability?

.. _SPD Metadata: https://docs.google.com/spreadsheets/d/1lee0yKDzclWyTMFDiN2DIw5HWvB6GWJwJyQrR7x733g/edit?usp=sharing
