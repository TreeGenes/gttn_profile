<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_user_form(&$form, &$form_state, &$form_id) {

  if (!empty($form_state['user'])) {
    $user = $form_state['user'];
  }
  else {
    global $user;
  }
  // Remove the "username" field. Email will be used instead.
  unset($form['account']['name']);

  $form['full_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Full name:',
  );

  $options = array();
  $orgs = db_select('gttn_profile_organization', 'o')
    ->fields('o', array('organization_id', 'name'))
    ->execute();

  while (($org = $orgs->fetchObject())) {
    $options[$org->organization_id] = $org->name;
  }

  if (!empty($options)) {
    // Create the organization checkboxes field.
    $form['org'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Please indicate the organizations you are part of:',
      '#options' => $options,
    );
  }

  $and = db_and()
    ->condition('uid', $user->uid)
    ->condition('status', 0);
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('organization_id'))
    ->condition($and)
    ->execute();

  if (!empty($query->fetchObject())) {
    $form['account']['status']['#disabled'] = TRUE;
    $form['account']['status']['#description'] = 'Not all of the organizations that this user is claiming membership of have approved the user.';
  }

  gttn_profile_user_form_defaults($form, $form_state, $form_id);
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
}

/**
 *
 */
function gttn_profile_user_form_defaults(&$form, &$form_state, &$form_id) {
  if (!empty($form_state['user'])) {
    $user = $form_state['user'];
  }
  else {
    global $user;
  }

  if ($user->uid == 0) {
    return;
  }

  $query = db_select('gttn_profile_user_chado', 'u');
  $query->join('chado.contact', 'c', 'c.contact_id = u.contact_id');
  $query->fields('c', array('name'))
    ->condition('uid', $user->uid);

  $form['full_name']['#default_value'] = $query->execute()->fetchObject()->name;

  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('organization_id'))
    ->condition('uid', $user->uid)
    ->execute();

  while (($org = $query->fetchObject())) {
    $form['org'][$org->organization_id]['#default_value'] = $org->organization_id;
  }
}
