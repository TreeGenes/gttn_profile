<?php

/**
 * @file
 */

/**
 * This function creates the add organization form and references the common
 * organization information from gttn_profile_organization_info().
 *
 * @param array $form
 *   The form being created.
 * @param array $form_state
 *   The state of the form being created.
 *
 * @return array
 */
function gttn_profile_organization_add($form, &$form_state, $organization_id = NULL) {

  global $user;

  if (!isset($user->mail)) {
    $destination = drupal_get_destination();
    drupal_goto('user/login', array('query' => $destination));
  }

  $form['consent_checks'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    'authorized_consent' => array(
      '#type' => 'checkbox',
      '#title' => t('By checking this box I confirm that I am an authorized representative of the laboratory for which information is provided in this database record.'),
    ),
    'lab_consent' => array(
      '#type' => 'checkbox',
      '#title' => t('By checking this box, on behalf of the laboratory for which information is declared in this database record, I agree to the Service Provider\'s Agreement. I understand that I can revisit and update any of the laboratory information contained in the record at any time for as long as the Service Provider Directory will be provided as a service. The laboratory information may be shared with third parties who are directly cooperating with GTTN.'),
    ),
    'indiv_consent' => array(
      '#type' => 'checkbox',
      '#title' => t('By checking this box:') . '<br>' . t('1) I confirm that my personal information [first name, last name, job title, email address, telephone number, date of birth, nationality] provided for the Service Providers Directory is correct.') . '<br>' . t('2) I understand and agree that my personal information will be processed by European Forest Institute, but that it will not be accessible to users of the Service Providers Directory. The personal data will be processed to contact the laboratory with service requests, generated via the Service Providers Directory, however without showing any peronal details to the users of the directory.') . '<br>' . t('3) I will remove my personal information as soon as my employment or affiliation with the laboratory ceases.') . '<br><br>' . t('My personal information may be shared with third parties who are directly involved with GTTN.'),
    ),
  );

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Information:'),
    '#collapsible' => TRUE,
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#required' => TRUE,
    ),
    'address_1' => array(
      '#type' => 'textfield',
      '#title' => t('Address Line 1'),
      '#required' => TRUE,
    ),
    'address_2' => array(
      '#type' => 'textfield',
      '#title' => t('Address Line 2'),
    ),
    'country' => array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#required' => TRUE,
    ),
    'city' => array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#required' => TRUE,
    ),
    'region' => array(
      '#type' => 'textfield',
      '#title' => t('State/Province'),
    ),
    'postal_code' => array(
      '#type' => 'textfield',
      '#title' => t('Postal Code'),
      '#required' => TRUE,
    ),
    'website' => array(
      '#type' => 'textfield',
      '#title' => t('Website'),
      '#required' => TRUE,
    ),
    'contact' => array(
      '#type' => 'textfield',
      '#title' => t('Primary Contact'),
      '#required' => TRUE,
      '#autocomplete_path' => 'gttn_profile_contact/autocomplete',
    ),
  );

  $years = array();
  for ($i = 0; $i <= 30; $i++) {
    $years[] = $i;
  }
  $years[] = '>30';
  $form['experience'] = array(
    '#type' => 'select',
    '#title' => t('Years of experience:'),
    '#options' => $years,
  );

  $form['court_case'] = array(
    '#type' => 'select',
    '#title' => t('Has this organization given evidence in a court case before?'),
    '#options' => array(
      'No' => 'No',
      'Yes' => 'Yes',
    ),
  );

  $form['samples_per_week'] = array(
    '#type' => 'select',
    '#title' => t('How many samples per week can the organization support?'),
    '#options' => array(
      '1-9' => '1-9',
      '10-49' => '10-49',
      '50-99' => '50-99',
      '100-499' => '100-499',
      '500+' => '500+',
    ),
  );

  $form['sample_turnaround'] = array(
    '#type' => 'select',
    '#title' => t('How many business days on average does it take to perform analysis?'),
    '#options' => array(
      '1-2' => '1-2',
      '3-5' => '3-5',
      '5-10' => '5-10',
      '11-15' => '11-15',
      '>15' => '>15',
    ),
  );

  $organizations = array();
  $query = db_select('gttn_profile_organization', 'o')
    ->fields('o', array('organization_id', 'name'));
  if (!empty($organization_id)) {
    $query->condition('organization_id', $organization_id, '!=');
  }
  $query = $query->execute();
  while (($org = $query->fetchObject())) {
    $organizations[$org->organization_id] = $org->name;
  }

  if (!empty($organizations)) {
    $form['partners'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Partner Labs:'),
      '#options' => $organizations,
    );
  }

  $form['blind_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('This organization is willing to participate in blind tests to validate testing capability.'),
  );

  // TODO: method types (genetic, isotope, anatomy, etc.)
  // Will also need to update SPD schema to support method types.
  $query = db_select('gttn_profile_method', 'm')
    ->fields('m', array('method_id', 'name'))
    ->execute();
  $methods = array();
  while (($method = $query->fetchObject())) {
    $methods[$method->method_id] = $method->name;
  }

  $form['methods'] = array(
    '#type' => 'fieldset',
    '#title' => t('Supported Analysis Methods'),
    '#collapsible' => TRUE,
  );

  $form['methods']['methods'] = array(
    '#type' => 'checkboxes',
    '#options' => $methods,
    '#ajax' => array(
      'callback' => 'gttn_profile_species_callback',
      'wrapper' => 'species-wrapper',
    ),
  );

  $query = db_select('gttn_profile_product', 'p')
    ->fields('p', array('product_id', 'name'))
    ->execute();
  $products = array();
  while (($product = $query->fetchObject())) {
    $products[$product->product_id] = $product->name;
  }

  $form['products'] = array(
    '#type' => 'fieldset',
    '#title' => t('Supported Products'),
    '#collapsible' => TRUE,
  );

  $form['products']['products'] = array(
    '#type' => 'checkboxes',
    '#options' => $products,
    '#ajax' => array(
      'wrapper' => 'species-wrapper',
      'callback' => 'gttn_profile_species_callback',
    ),
  );

  $query = db_select('gttn_profile_region', 'r')
    ->fields('r', array('region_id', 'name'))
    ->execute();
  $regions = array();
  while (($region = $query->fetchObject())) {
    $regions[$region->region_id] = $region->name;
  }

  $form['regions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Supported Regions'),
    '#collapsible' => TRUE,
  );

  $form['regions']['regions'] = array(
    '#type' => 'checkboxes',
    '#options' => $regions,
    '#ajax' => array(
      'wrapper' => 'species-wrapper',
      'callback' => 'gttn_profile_species_callback',
    ),
  );

  $form['species'] = array(
    '#type' => 'fieldset',
    '#title' => 'Supported Genus/Species',
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );

  if (isset($form_state['triggering_element'])) {
    if (isset($form_state['values']['species']['number']) and $form_state['triggering_element']['#name'] == 'Add Genus/Species') {
      $form_state['values']['species']['number']++;
    }
    elseif (isset($form_state['values']['species']['number']) and $form_state['triggering_element']['#name'] == 'Remove Genus/Species' and $form_state['values']['species']['number'] > 1) {
      $form_state['values']['species']['number']--;
    }
  }

  $org_number = isset($form_state['values']['species']['number']) ? $form_state['values']['species']['number'] : NULL;

  if (!isset($org_number)) {
    $org_number = 1;
  }

  $form['species']['#prefix'] = '<div id="species-wrapper">';
  $form['species']['#suffix'] = '</div>';

  $form['species']['add'] = array(
    '#type' => 'button',
    '#title' => t('Add Genus/Species'),
    '#button_type' => 'button',
    '#value' => t('Add Genus/Species'),
    '#name' => t('Add Genus/Species'),
    '#ajax' => array(
      'wrapper' => 'species-wrapper',
      'callback' => 'gttn_profile_species_callback',
    ),
  );

  $form['species']['remove'] = array(
    '#type' => 'button',
    '#title' => t('Remove Genus/Species'),
    '#button_type' => 'button',
    '#value' => t('Remove Genus/Species'),
    '#name' => t('Remove Genus/Species'),
    '#ajax' => array(
      'wrapper' => 'species-wrapper',
      'callback' => 'gttn_profile_species_callback',
    ),
  );

  $form['species']['number'] = array(
    '#type' => 'hidden',
    '#value' => $org_number,
  );

  $selected_methods = array();
  $selected_products = array();
  $selected_regions = array();
  if (!empty($form_state['values']['methods'])) {
    foreach ($methods as $method_id => $method_name) {
      if (isset($form_state['values']['methods'][$method_id]) and $form_state['values']['methods'][$method_id]) {
        $selected_methods[$method_id] = $method_name;
      }
    }
    foreach ($products as $product_id => $product_name) {
      if (isset($form_state['values']['products'][$product_id]) and $form_state['values']['products'][$product_id]) {
        $selected_products[$product_id] = $product_name;
      }
    }
    foreach ($regions as $region_id => $region_name) {
      if (isset($form_state['values']['regions'][$region_id]) and $form_state['values']['regions'][$region_id]) {
        $selected_regions[$region_id] = $region_name;
      }
    }
  }

  for ($i = 1; $i <= $org_number; $i++) {
    $form['species'][$i] = array(
      '#type' => 'fieldset',
    );
    $form['species'][$i]['name'] = array(
      '#type' => 'textfield',
      '#autocomplete_path' => 'gttn_profile_species/autocomplete',
    );
    $form['species'][$i]['inclusive'] = array(
      '#type' => 'checkbox',
      '#title' => t('This organization supports all selected analysis methods on all selected products and regions for this species.'),
      '#ajax' => array(
        'callback' => 'gttn_profile_species_callback',
        'wrapper' => 'species-wrapper',
      ),
      '#default_value' => isset($form_state['values']['species'][$i]['inclusive']) ? $form_state['values']['species'][$i]['inclusive'] : TRUE,
    );
    if (isset($form_state['values']['species'][$i]['inclusive']) and !$form_state['values']['species'][$i]['inclusive']) {
      if (!empty($selected_methods)) {
        $form['species'][$i]['methods'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Supported analysis methods for this species:'),
          '#options' => $selected_methods,
        );
      }
      if (!empty($selected_products)) {
        $form['species'][$i]['products'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Supported products for this species:'),
          '#options' => $selected_products,
        );
      }
      if (!empty($selected_regions)) {
        $form['species'][$i]['regions'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Supported regions for this species:'),
          '#options' => $selected_regions,
        );
      }
    }
  }

  $form['isotope'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );

  $form['isotope']['iso17025'] = array(
    '#type' => 'checkbox',
    '#title' => t('This organization has an ISO17025 Accreditation.'),
  );

  $form['isotope']['accreditations'] = array(
    '#type' => 'textfield',
    '#title' => t('Please list any other accreditations here:'),
  );

  $form['isotope']['num_signatures'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Isotope Signatures'),
  );

  $form['isotope']['standards'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Supported Isotope Standards'),
    '#options' => array(
      'Standard 1' => 'Standard 1',
      'Standard 2' => 'Standard 2',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#prefix'] = '<div id="organization-add-form">';
  $form['#suffix'] = '</div>';

  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  drupal_add_css(drupal_get_path('module', 'gttn_profile') . '/css/style.css');

  return $form;
}

/**
 * Implements hook_form_validate().
 *
 * This function checks that the organization provided during the add form is valid.
 *
 * @param array $form
 *   The form being validated.
 * @param array $form_state
 *   The state of the form being validated.
 */
function gttn_profile_organization_add_validate(&$form, &$form_state) {

  if ($form_state['submitted'] == '1') {

    foreach ($form_state['values']['consent_checks'] as $key => $val) {
      if (empty($val)) {
        form_set_error("consent_checks][$key", "Consent check: field is required.");
      }
    }

    $result = chado_select_record('contact', array('contact_id'), array(
      'name' => $form_state['values']['contact'],
      'type_id' => array(
        'name' => 'Person',
        'cv_id' => array(
          'name' => 'tripal_contact',
        ),
      ),
    ));

    // If contact name was not in chado.contact, throw error.
    if (empty($result)) {
      form_set_error('contact', 'Primary Contact: please choose a primary contact from the autocomplete list.');
    }

    for ($i = 1; $i <= $form_state['values']['species']['number']; $i++) {
      $name = $form_state['values']['species'][$i]['name'];
      if ($name == '') {
        form_set_error("species][$i", "Genus/Species $i: field is required.");
      }
      else {
        $name = explode(" ", $name);
        $genus = $name[0];
        $species = implode(" ", array_slice($name, 1));
        $name = implode(" ", $name);
        $empty_pattern = '/^ *$/';
        $correct_pattern = '/^[A-Z|a-z|.| |-]*$/';
        if (!isset($genus) or !isset($species) or preg_match($empty_pattern, $genus) or !preg_match($correct_pattern, $genus) or !preg_match($correct_pattern, $species)) {
          form_set_error("species][$i", check_plain("Genus/Species $i: Field should contain only letters, spaces, and dots."));
        }
      }
    }
  }
  else {
    drupal_get_messages('error');
    form_clear_error();
  }
}

/**
 * Implements hook_form_submit().
 *
 * This function submits the form data into the database and references the
 * common organization information from gttn_profile_organization_info().
 *
 * @param array $form
 *   The form being submitted.
 * @param array $form_state
 *   The state of the form being submitted.
 */
function gttn_profile_organization_add_submit($form, &$form_state, $org_id = NULL) {
  $organization_id = $org_id;
  $vals = $form_state['values'];

  $org_record = array(
    'name' => $vals['name'],
    'address_1' => $vals['address_1'],
    'address_2' => $vals['address_2'],
    'country' => $vals['country'],
    'city' => $vals['city'],
    'region' => $vals['region'],
    'postal_code' => $vals['postal_code'],
    'website' => $vals['website'],
  );

  if (!empty($organization_id)) {
    $org_record['organization_id'] = $organization_id;
  }
  $organization_id = gttn_profile_create_record('gttn_profile_organization', $org_record);

  $selected_methods = array();
  foreach ($vals['methods'] as $method_id) {
    if ($method_id) {
      $selected_methods[] = $method_id;
    }
  }
  $selected_products = array();
  foreach ($vals['products'] as $product_id) {
    if ($product_id) {
      $selected_products[] = $product_id;
    }
  }
  $selected_regions = array();
  foreach ($vals['regions'] as $region_id) {
    if ($region_id) {
      $selected_regions[] = $region_id;
    }
  }

  for ($i = 1; $i <= $vals['species']['number']; $i++) {
    $species_name = $vals['species'][$i]['name'];
    $name = explode(" ", $species_name);
    $genus = $name[0];
    $species = implode(" ", array_slice($name, 1));
    $species_id = gttn_profile_create_record('chado.organism', array(
      'genus' => $genus,
      'species' => $species,
      'type_id' => array(
        'name' => 'organism',
        'cv_id' => array(
          'name' => 'obi',
        ),
      ),
    ));

    if ($vals['species'][$i]['inclusive']) {
      $methods = $selected_methods;
      $products = $selected_products;
      $regions = $selected_regions;
    }
    else {
      $methods = array();
      foreach ($vals['species'][$i]['methods'] as $method) {
        if ($method) {
          $methods[] = $method;
        }
      }
      $products = array();
      foreach ($vals['species'][$i]['products'] as $product) {
        if ($product) {
          $products[] = $product;
        }
      }
      $regions = array();
      foreach ($vals['species'][$i]['regions'] as $region) {
        if ($region) {
          $regions[] = $region;
        }
      }
    }
    foreach ($methods as $method_id) {
      foreach ($products as $product_id) {
        foreach ($regions as $region_id) {
          gttn_profile_create_record('gttn_profile_support', array(
            'organization_id' => $organization_id,
            'species_id' => $species_id,
            'method_id' => $method_id,
            'product_id' => $product_id,
            'region_id' => $region_id,
          ));
        }
      }
    }
  }

  $name = $vals['contact'];
  $query = db_select('chado.contact', 'c');
  $query->join('gttn_profile_user_chado', 'u', 'u.contact_id = c.contact_id');
  $query->fields('u', array('uid', 'contact_id'))
    ->condition('c.name', $name);
  $result = $query->execute()->fetchObject();

  if (!isset($org_id)) {
    gttn_profile_create_record('gttn_profile_organization_members', array(
      'organization_id' => $organization_id,
      'uid' => $result->uid,
      'contact_id' => $result->contact_id,
      'is_primary_contact' => TRUE,
      'status' => 1,
    ));
  }

  gttn_profile_create_record('gttn_profile_organization_property', array(
    'organization_id' => $organization_id,
    'property_id' => variable_get('gttn_profile_property_experience'),
    'value' => $vals['experience'],
  ));

  if ($vals['court_case'] == 'Yes') {
    gttn_profile_create_record('gttn_profile_organization_property', array(
      'organization_id' => $organization_id,
      'property_id' => variable_get('gttn_profile_property_evidence_given'),
      'value' => "",
    ));
  }

  gttn_profile_create_record('gttn_profile_organization_property', array(
    'organization_id' => $organization_id,
    'property_id' => variable_get('gttn_profile_property_samples_weekly'),
    'value' => $vals['samples_per_week'],
  ));

  gttn_profile_create_record('gttn_profile_organization_property', array(
    'organization_id' => $organization_id,
    'property_id' => variable_get('gttn_profile_property_samples_turnaround'),
    'value' => $vals['sample_turnaround'],
  ));

  if (!empty($vals['partners'])) {
    foreach ($vals['partners'] as $partner_id => $check) {
      if ($check) {
        gttn_profile_create_record('gttn_profile_organization_property', array(
          'organization_id' => $organization_id,
          'property_id' => variable_get('gttn_profile_property_partner_organization'),
          'value' => $partner_id,
        ));
      }
    }
  }

  if (!empty($vals['blind_test'])) {
    gttn_profile_create_record('gttn_profile_organization_property', array(
      'organization_id' => $organization_id,
      'property_id' => variable_get('gttn_profile_property_blind_test'),
    ));
  }

  if ($vals['iso17025']) {
    gttn_profile_create_record('gttn_profile_organization_property', array(
      'organization_id' => $organization_id,
      'property_id' => variable_get('gttn_profile_property_isotope_accreditation'),
      'value' => 'ISO17025',
    ));
  }

  if (!empty($vals['accreditations'])) {
    gttn_profile_create_record('gttn_profile_organization_property', array(
      'organization_id' => $organization_id,
      'property_id' => variable_get('gttn_profile_property_isotope_accreditation'),
      'value' => $vals['accreditations'],
    ));
  }

  if (!empty($vals['num_signatures'])) {
    gttn_profile_create_record('gttn_profile_organization_property', array(
      'organization_id' => $organization_id,
      'property_id' => variable_get('gttn_profile_property_num_signatures'),
      'value' => $vals['num_signatures'],
    ));
  }

  foreach ($vals['standards'] as $check) {
    if ($check) {
      gttn_profile_create_record('gttn_profile_organization_property', array(
        'organization_id' => $organization_id,
        'property_id' => variable_get('gttn_profile_property_isotope_standard'),
        'value' => $check,
      ));
    }
  }

  drupal_goto('organization/directory');
}

/**
 * This function creates the edit organization form.
 *
 * @param array $form
 *   The form being created.
 * @param array $form_state
 *   The state of the form being created.
 * @param int $org_id
 *   The id of the organization being edited.
 *
 * @return array $form
 */
function gttn_profile_organization_edit($form, &$form_state, $org_id) {
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o')
    ->condition('organization_id', $org_id)
    ->execute()->fetchObject();

  if (empty($form_state['values'])) {
    $species_arr = db_select('gttn_profile_support', 's')
      ->distinct()
      ->fields('s', array('species_id'))
      ->condition('organization_id', $org_id)
      ->execute()->fetchAll();

    foreach ($species_arr as $key => $result) {
      $species_cross_product = db_query('select count(distinct(method_id)) * '
                . 'count(distinct(product_id)) * '
                . 'count(distinct(region_id)) as a '
                . 'from gttn_profile_support '
                . "where species_id = {$result->species_id};")->fetchObject()->a;
      $org_cross_product = db_query('select count(distinct(method_id)) * '
                . 'count(distinct(product_id)) * '
                . 'count(distinct(region_id)) as a '
                . 'from gttn_profile_support '
                . "where organization_id = $org_id;")->fetchObject()->a;
      $form_state['values']['species'][$key + 1]['inclusive'] = $species_cross_product == $org_cross_product;
    }

    $form_state['values']['species']['number'] = count($species_arr);

    $query = db_select('gttn_profile_support', 's')
      ->distinct()
      ->fields('s', array('method_id'))
      ->condition('organization_id', $org_id)
      ->execute();

    while (($result = $query->fetchObject())) {
      $form_state['values']['methods'][$result->method_id] = $result->method_id;
    }

    $query = db_select('gttn_profile_support', 's')
      ->distinct()
      ->fields('s', array('product_id'))
      ->condition('organization_id', $org_id)
      ->execute();

    while (($result = $query->fetchObject())) {
      $form_state['values']['products'][$result->product_id] = $result->product_id;
    }

    $query = db_select('gttn_profile_support', 's')
      ->distinct()
      ->fields('s', array('region_id'))
      ->condition('organization_id', $org_id)
      ->execute();

    while (($result = $query->fetchObject())) {
      $form_state['values']['regions'][$result->region_id] = $result->region_id;
    }
  }

  $form = gttn_profile_organization_add($form, $form_state, $org_id);

  $form['oid'] = array(
    '#type' => 'hidden',
    '#value' => $org_id,
  );

  foreach ($form['general'] as $key => $val) {
    if (is_array($val) and array_key_exists('#type', $val) and property_exists($org, $key)) {
      $form['general'][$key]['#default_value'] = $org->{$key};
    }
  }

  $and = db_and()
    ->condition('organization_id', $org_id)
    ->condition('is_primary_contact', TRUE);
  $primary_id = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('contact_id'))
    ->condition($and)
    ->execute()->fetchObject()->contact_id;
  $primary_name = db_select('chado.contact', 'c')
    ->fields('c', array('name'))
    ->condition('contact_id', $primary_id)
    ->execute()->fetchObject()->name;

  $form['general']['contact']['#default_value'] = $primary_name;

  $query = db_select('gttn_profile_organization_property', 'o')
    ->fields('o', array('organization_property_id', 'property_id', 'value'))
    ->condition('organization_id', $org_id)
    ->execute();
  while (($result = $query->fetchObject())) {
    switch ($result->property_id) {
      case variable_get('gttn_profile_property_experience'):
        $form['experience']['#default_value'] = $result->value;
        break;

      case variable_get('gttn_profile_property_evidence_given'):
        $form['court_case']['#default_value'] = 'Yes';
        break;

      case variable_get('gttn_profile_property_samples_weekly'):
        $form['samples_per_week']['#default_value'] = $result->value;
        break;

      case variable_get('gttn_profile_property_samples_turnaround'):
        $form['sample_turnaround']['#default_value'] = $result->value;
        break;

      case variable_get('gttn_profile_property_partner_organization'):
        $form['partners'][$result->value]['#default_value'] = $result_value;
        break;

      case variable_get('gttn_profile_property_isotope_accreditation'):
        if ($result->value == 'ISO17025') {
          $form['isotope']['iso17025']['#default_value'] = TRUE;
        }
        else {
          $form['isotope']['accreditations']['#default_value'] = $result->value;
        }
        break;

      case variable_get('gttn_profile_property_num_signatures'):
        $form['isotope']['num_signatures']['#default_value'] = $result->value;
        break;

      case variable_get('gttn_profile_property_isotope_standard'):
        $form['isotope']['standards'][$result->value]['#default_value'] = $result->value;
        break;

      default:
        break;
    }
  }

  $query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('method_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  while (($result = $query->fetchObject())) {
    $form['methods']['methods'][$result->method_id]['#default_value'] = $result->method_id;
  }

  $query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('product_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  while (($result = $query->fetchObject())) {
    $form['products']['products'][$result->product_id]['#default_value'] = $result->product_id;
  }

  $query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('region_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  while (($result = $query->fetchObject())) {
    $form['regions']['regions'][$result->region_id]['#default_value'] = $result->region_id;
  }

  foreach ($species_arr as $key => $result) {
    $organism = db_select('chado.organism', 'o')
      ->fields('o', array('genus', 'species'))
      ->condition('organism_id', $result->species_id)
      ->range(0, 1)
      ->execute()->fetchObject();
    $form['species'][$key + 1]['name']['#default_value'] = "{$organism->genus} {$organism->species}";

    if (!$form_state['values']['species'][$key + 1]['inclusive']) {
      foreach ($form['species'][$key + 1]['methods']['#options'] as $method_id => $method_name) {
        if (db_select('gttn_profile_support', 's')
          ->fields('s', array('support_id'))
          ->condition(db_and()
            ->condition('organization_id', $org_id)
            ->condition('species_id', $result->species_id)
            ->condition('method_id', $method_id))
          ->range(0, 1)
          ->execute()->fetchObject()) {
          $form['species'][$key + 1]['methods'][$method_id]['#default_value'] = $method_id;
        }
      }

      foreach ($form['species'][$key + 1]['products']['#options'] as $product_id => $product_name) {
        if (db_select('gttn_profile_support', 's')
          ->fields('s', array('support_id'))
          ->condition(db_and()
            ->condition('organization_id', $org_id)
            ->condition('species_id', $result->species_id)
            ->condition('product_id', $product_id))
          ->range(0, 1)
          ->execute()->fetchObject()) {
          $form['species'][$key + 1]['products'][$product_id]['#default_value'] = $product_id;
        }
      }

      foreach ($form['species'][$key + 1]['regions']['#options'] as $region_id => $region_name) {
        if (db_select('gttn_profile_support', 's')
          ->fields('s', array('support_id'))
          ->condition(db_and()
            ->condition('organization_id', $org_id)
            ->condition('species_id', $result->species_id)
            ->condition('region_id', $region_id))
          ->range(0, 1)
          ->execute()->fetchObject()) {
          $form['species'][$key + 1]['regions'][$region_id]['#default_value'] = $region_id;
        }
      }
    }
  }

  return $form;
}

/**
 * Implements hook_form_validate().
 *
 * This function checks that the information provided during the edit organization
 * form is valid. It depends on the validate function from the add form.
 *
 * @param array $form
 *   The form being validated.
 * @param array $form_state
 *   The state of the form being validated.
 */
function gttn_profile_organization_edit_validate($form, &$form_state) {
  gttn_profile_organization_add_validate($form, $form_state);
}

/**
 * Implements hook_form_submit().
 *
 * This function submits the form data into the database and references the
 * common organization information from gttn_profile_organization_info().
 *
 * @param array $form
 *   The form being submitted.
 * @param array $form_state
 *   The state of the form being submitted.
 */
function gttn_profile_organization_edit_submit($form, &$form_state) {
  $org_id = $form_state['values']['oid'];
  // TODO.
  db_query("delete from gttn_profile_organization_property where organization_id = $org_id");
  db_query("delete from gttn_profile_support where organization_id = $org_id");
  gttn_profile_organization_add_submit($form, $form_state, $org_id);
}

/**
 *
 */
function gttn_profile_species_callback(&$form, $form_state) {
  return $form['species'];
}
