<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_service_request($form, &$form_state) {

  $form['species'] = array(
    '#type' => 'textfield',
    '#title' => t('Species:'),
    '#autocomplete_path' => 'gttn_profile_species/autocomplete',
  );

  $methods_query = db_select('gttn_profile_method', 'm')
    ->distinct()
    ->fields('m', array('method_id', 'name'))
    ->execute();

  $form['methods'] = array(
    '#type' => 'select',
    '#title' => t('Methods:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($method = $methods_query->fetchObject())) {
    $form['methods']['#options'][$method->method_id] = $method->name;
  }

  $products_query = db_select('gttn_profile_product', 'p')
    ->distinct()
    ->fields('p', array('product_id', 'name'))
    ->execute();

  $form['products'] = array(
    '#type' => 'select',
    '#title' => t('Products:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($product = $products_query->fetchObject())) {
    $form['products']['#options'][$product->product_id] = $product->name;
  }

  $region_query = db_select('gttn_profile_region', 'r')
    ->distinct()
    ->fields('r', array('region_id', 'name'))
    ->execute();

  $form['regions'] = array(
    '#type' => 'select',
    '#title' => t('Regions:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($region = $region_query->fetchObject())) {
    $form['regions']['#options'][$region->region_id] = $region->name;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');

  return $form;
}

/**
 *
 */
function gttn_profile_service_request_validate(&$form, &$form_state) {
  if ($form_state['submitted'] == '1') {

  }
}

/**
 *
 */
function gttn_profile_service_request_submit($form, &$form_state) {
  $orgs = gttn_profile_service_filter($form, $form_state);
  drupal_goto('service_candidates', array(
    'query' => array('orgs' => implode(';', $orgs)),
  ));
}
